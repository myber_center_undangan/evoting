/*
 Navicat Premium Data Transfer

 Source Server         : DB
 Source Server Type    : MySQL
 Source Server Version : 100425
 Source Host           : localhost:3306
 Source Schema         : e-voting

 Target Server Type    : MySQL
 Target Server Version : 100425
 File Encoding         : 65001

 Date: 27/01/2023 10:13:38
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for calon
-- ----------------------------
DROP TABLE IF EXISTS `calon`;
CREATE TABLE `calon`  (
  `id_calon` int(11) NOT NULL,
  `nama` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `idevoting_fk` int(11) NULL DEFAULT NULL,
  `foto` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL,
  PRIMARY KEY (`id_calon`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of calon
-- ----------------------------
INSERT INTO `calon` VALUES (1, 'Zaki - Dimas', 1, 'paslon1.jpg');
INSERT INTO `calon` VALUES (2, 'Wahyu - Faruq', 1, 'paslon2.jpg');
INSERT INTO `calon` VALUES (3, 'Nisrina - Farhana', 2, 'paslon1p.jpg');
INSERT INTO `calon` VALUES (4, 'Auha May - Sania', 2, 'paslon2p.jpg');

-- ----------------------------
-- Table structure for evoting
-- ----------------------------
DROP TABLE IF EXISTS `evoting`;
CREATE TABLE `evoting`  (
  `id_evoting` int(11) NOT NULL AUTO_INCREMENT,
  `kode` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL,
  `nama` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL,
  PRIMARY KEY (`id_evoting`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of evoting
-- ----------------------------
INSERT INTO `evoting` VALUES (1, 'p1', 'E-Voting Osis Putra Vohisa');
INSERT INTO `evoting` VALUES (2, 'p2', 'E-Voting Osis Putri Vohisa');

-- ----------------------------
-- Table structure for kelas
-- ----------------------------
DROP TABLE IF EXISTS `kelas`;
CREATE TABLE `kelas`  (
  `id_kelas` int(11) NOT NULL AUTO_INCREMENT,
  `kelas` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL,
  PRIMARY KEY (`id_kelas`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 17 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of kelas
-- ----------------------------
INSERT INTO `kelas` VALUES (1, 'X TKJ 1');
INSERT INTO `kelas` VALUES (2, 'X TKJ 2');
INSERT INTO `kelas` VALUES (3, 'X TKJ 2 PI');
INSERT INTO `kelas` VALUES (4, 'X DKV 1');
INSERT INTO `kelas` VALUES (5, 'X DKV 2');
INSERT INTO `kelas` VALUES (6, 'X APHP');
INSERT INTO `kelas` VALUES (7, 'XI TKJ 1');
INSERT INTO `kelas` VALUES (8, 'XI TKJ 1 PI');
INSERT INTO `kelas` VALUES (9, 'XI DKV 1');
INSERT INTO `kelas` VALUES (10, 'XI DKV 1 PI');
INSERT INTO `kelas` VALUES (11, 'XI APHP');
INSERT INTO `kelas` VALUES (12, 'XII TKJ 1');
INSERT INTO `kelas` VALUES (13, 'XII TKJ 2');
INSERT INTO `kelas` VALUES (14, 'XII TKJ 3');
INSERT INTO `kelas` VALUES (15, 'XII APHP');
INSERT INTO `kelas` VALUES (16, 'XII APHP PA');

-- ----------------------------
-- Table structure for kode_ref
-- ----------------------------
DROP TABLE IF EXISTS `kode_ref`;
CREATE TABLE `kode_ref`  (
  `id_kode_ref` int(11) NOT NULL AUTO_INCREMENT,
  `kode` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `idevoting_fk` int(11) NULL DEFAULT NULL,
  `siswa` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL,
  `idkelas_fk` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id_kode_ref`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 227 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of kode_ref
-- ----------------------------
INSERT INTO `kode_ref` VALUES (1, '13498', 1, 'ABDU RAHMAN FANSYAH', 1);
INSERT INTO `kode_ref` VALUES (2, '25252', 1, 'ABDUL HALIM RADITYANSYAH', 1);
INSERT INTO `kode_ref` VALUES (3, '44507', 1, 'ACHMAD FALIH SABILARROSYAD', 1);
INSERT INTO `kode_ref` VALUES (4, '92913', 1, 'ACHMAD MUSTOFA', 1);
INSERT INTO `kode_ref` VALUES (5, '24243', 1, 'AHMAD AKIA RAIHAN ILYASA', 1);
INSERT INTO `kode_ref` VALUES (6, '11410', 1, 'AHMAD DANI LASMANA', 1);
INSERT INTO `kode_ref` VALUES (7, '33855', 1, 'AHMAD FIRQI', 1);
INSERT INTO `kode_ref` VALUES (8, '48035', 1, 'AHMAD HABIBUROSYAD', 1);
INSERT INTO `kode_ref` VALUES (9, '78753', 1, 'AHMAD NUR ROFI\'UDDIN', 1);
INSERT INTO `kode_ref` VALUES (10, '46546', 1, 'AJIB BAHRUL IHSAN', 1);
INSERT INTO `kode_ref` VALUES (11, '71567', 1, 'ANDIKA BAYU NUGROHO', 1);
INSERT INTO `kode_ref` VALUES (12, '42999', 1, 'BIMO FEREDI', 1);
INSERT INTO `kode_ref` VALUES (13, '38739', 1, 'DIMAS PRAWIRO GHIFARI', 1);
INSERT INTO `kode_ref` VALUES (14, '19303', 1, 'FAHMI ZAINUL MUTTAQIN', 1);
INSERT INTO `kode_ref` VALUES (15, '63077', 1, 'FAREL ANDARU ADJI PAMUNGKAS', 1);
INSERT INTO `kode_ref` VALUES (16, '66795', 1, 'FAREL AZZAHARA AHMADA', 1);
INSERT INTO `kode_ref` VALUES (17, '23000', 1, 'FATHUN NAJA', 1);
INSERT INTO `kode_ref` VALUES (18, '50099', 1, 'JEVON ARIYANTO', 1);
INSERT INTO `kode_ref` VALUES (19, '40000', 1, 'M. ALFIATUR ROHMAN', 1);
INSERT INTO `kode_ref` VALUES (20, '40438', 1, 'M. BAGAS FATURRAHMAN', 1);
INSERT INTO `kode_ref` VALUES (21, '42605', 1, 'M. KHOLILUR ROHMAN', 1);
INSERT INTO `kode_ref` VALUES (22, '15843', 1, 'M. KHOMSUN FADLI', 1);
INSERT INTO `kode_ref` VALUES (23, '14960', 1, 'MUHAMMAD RABBANI ARDIANSYAH', 1);
INSERT INTO `kode_ref` VALUES (24, '87991', 1, 'QODIMUL ZAKWAN MUCHTAR', 1);
INSERT INTO `kode_ref` VALUES (25, '56130', 1, 'AHMAD RAYHAN ARDHANI PUTRA', 1);
INSERT INTO `kode_ref` VALUES (26, '47382', 1, 'AHMAD YUSNAF FAWWAZI', 1);
INSERT INTO `kode_ref` VALUES (27, '74566', 1, 'MAULANA FAIZ FIRMANSYAH', 2);
INSERT INTO `kode_ref` VALUES (28, '85402', 1, 'MOCHAMAD BASUKI RAHMAT', 2);
INSERT INTO `kode_ref` VALUES (29, '85403', 1, 'MOCHAMMAD MIFTAH ULUMIDDIN', 2);
INSERT INTO `kode_ref` VALUES (30, '47613', 1, 'MOH. ANGGA ARDIANSYAH', 2);
INSERT INTO `kode_ref` VALUES (31, '44589', 1, 'MOH. DAFFA SAFIRUZZAHIQ', 2);
INSERT INTO `kode_ref` VALUES (32, '31807', 1, 'MOH. SAFI', 2);
INSERT INTO `kode_ref` VALUES (33, '61797', 1, 'MOHAMMAD BURHANUDDIN', 2);
INSERT INTO `kode_ref` VALUES (34, '52918', 1, 'MUCHAMMAD ABDUL HADI', 2);
INSERT INTO `kode_ref` VALUES (35, '53522', 1, 'MUCHAMMAD ROYKHAN FIRDAUS', 2);
INSERT INTO `kode_ref` VALUES (36, '91902', 1, 'MUH MAULANA FAHMI RAMADAN', 2);
INSERT INTO `kode_ref` VALUES (37, '27710', 1, 'MUH. SAIFUDDIN ZAKARIA', 2);
INSERT INTO `kode_ref` VALUES (38, '71458', 1, 'MUHAMMAD ASHRUL', 2);
INSERT INTO `kode_ref` VALUES (39, '59987', 1, 'MUHAMMAD FAHRI HUSEIN', 2);
INSERT INTO `kode_ref` VALUES (40, '31310', 1, 'MUHAMMAD HASYIM MASHURI', 2);
INSERT INTO `kode_ref` VALUES (41, '86382', 1, 'MUHAMMAD KHOIRON KHOLES', 2);
INSERT INTO `kode_ref` VALUES (42, '56573', 1, 'R. FANDI MUDIAN PRADANA', 2);
INSERT INTO `kode_ref` VALUES (43, '73818', 1, 'RIDHO SAPUTRA', 2);
INSERT INTO `kode_ref` VALUES (44, '76330', 1, 'SYAHRUL MINAN', 2);
INSERT INTO `kode_ref` VALUES (45, '75537', 2, 'VANESA WILDAN AL-FATIH', 3);
INSERT INTO `kode_ref` VALUES (46, '89961', 2, 'FAZA ATI\'ILLAH', 3);
INSERT INTO `kode_ref` VALUES (47, '56603', 2, 'FISCHA HABIBAH', 3);
INSERT INTO `kode_ref` VALUES (48, '86150', 2, 'KAFFANA AL KHANANI', 3);
INSERT INTO `kode_ref` VALUES (49, '92218', 2, 'NABILA SHOFIYATUL AMELIA', 3);
INSERT INTO `kode_ref` VALUES (50, '57375', 2, 'NAILA FALENTINA NAHYATUL A.', 3);
INSERT INTO `kode_ref` VALUES (51, '90712', 2, 'RIZKY FAJAR WAHYUNI', 3);
INSERT INTO `kode_ref` VALUES (52, '85066', 2, 'ROZA FITRIANI WULANDARI', 3);
INSERT INTO `kode_ref` VALUES (53, '95158', 2, 'SALSABILA SEPVIA PUTRI', 3);
INSERT INTO `kode_ref` VALUES (54, '32648', 2, 'ZUHROTUS SANIIYAH', 3);
INSERT INTO `kode_ref` VALUES (55, '67352', 1, 'ABDUL MUN\'IM ZAM ZAMY', 4);
INSERT INTO `kode_ref` VALUES (56, '86711', 1, 'ABDULLOH ABID AL-QODRI', 4);
INSERT INTO `kode_ref` VALUES (57, '79593', 1, 'AHMAD ROZAK RABANI ', 4);
INSERT INTO `kode_ref` VALUES (58, '43932', 1, 'ALIF MUHAMMAD GATRA ABDULLAH', 4);
INSERT INTO `kode_ref` VALUES (59, '86316', 1, 'ARY ARDIANSYAH', 4);
INSERT INTO `kode_ref` VALUES (60, '40630', 1, 'DAMMAWUZ ZAINAL ALFATH', 4);
INSERT INTO `kode_ref` VALUES (61, '23966', 1, 'DEEDAD AL BALIYA', 4);
INSERT INTO `kode_ref` VALUES (62, '34664', 1, 'FALIH ZAIN DAIFULLAH', 4);
INSERT INTO `kode_ref` VALUES (63, '71626', 1, 'FIAN MAULIDAN HARDIYANTA', 4);
INSERT INTO `kode_ref` VALUES (64, '88263', 1, 'GALANG ULUL ALBAB', 4);
INSERT INTO `kode_ref` VALUES (65, '15375', 1, 'GILANG SATRIA RAMADHANI', 4);
INSERT INTO `kode_ref` VALUES (66, '95405', 1, 'IMAM AHMAD KHUDAEFI ALBAR', 4);
INSERT INTO `kode_ref` VALUES (67, '99369', 1, 'KHANSA AFRINANDA AGASI', 4);
INSERT INTO `kode_ref` VALUES (68, '63695', 1, 'M. AGUS SAIFUDIN ZUHRI', 4);
INSERT INTO `kode_ref` VALUES (69, '70779', 1, 'M. FAKHRUR RAMADHANI', 4);
INSERT INTO `kode_ref` VALUES (70, '97980', 1, 'M. IZZUDDIN RIFQI', 4);
INSERT INTO `kode_ref` VALUES (71, '51128', 1, 'MOCH. RAFY DENIS SYAHBANA', 4);
INSERT INTO `kode_ref` VALUES (72, '25712', 1, 'MUCHAMMAD AZAM FANANI', 4);
INSERT INTO `kode_ref` VALUES (73, '95001', 1, 'MUHAMMAD ADILAH RAYHAN', 4);
INSERT INTO `kode_ref` VALUES (74, '88110', 1, 'MUHAMMAD FAROUQ ABDUL GHONI', 4);
INSERT INTO `kode_ref` VALUES (75, '92971', 1, 'MUHAMMAD MAKAYLA AKBAR', 4);
INSERT INTO `kode_ref` VALUES (76, '72682', 1, 'MUHAMMAD RIDWAN KAUTSAR FATAHILLAH', 4);
INSERT INTO `kode_ref` VALUES (77, '43415', 1, 'MUHAMMAD SYAUQI', 4);
INSERT INTO `kode_ref` VALUES (78, '97987', 1, 'SULTAN BILAL AN-NAFI\'', 4);
INSERT INTO `kode_ref` VALUES (79, '47023', 1, 'TRIO MICKI GALIH SAPUTRA', 4);
INSERT INTO `kode_ref` VALUES (80, '37366', 1, 'ZUNNOON NAZRAN JANKI DAUSAT', 4);
INSERT INTO `kode_ref` VALUES (81, '20113', 2, 'AFRINA FAROH FIRJANI', 5);
INSERT INTO `kode_ref` VALUES (82, '26145', 2, 'ALIF ALFIYATUS SA\'DIYAH', 5);
INSERT INTO `kode_ref` VALUES (83, '14794', 2, 'AZZAH RATUL KHUSNIAH', 5);
INSERT INTO `kode_ref` VALUES (84, '62683', 2, 'DIHAN AMELIA PUTRI', 5);
INSERT INTO `kode_ref` VALUES (85, '96965', 2, 'DINDA AKTHA FEBIANA', 5);
INSERT INTO `kode_ref` VALUES (86, '53873', 2, 'DWI AFIFAH KHOIRUNISA', 5);
INSERT INTO `kode_ref` VALUES (87, '95164', 2, 'ELSA SALSABILA', 5);
INSERT INTO `kode_ref` VALUES (88, '99444', 2, 'FIRDA NADHIFAH MAULANA', 5);
INSERT INTO `kode_ref` VALUES (89, '85255', 2, 'HIMA TALIA EL-FURQONIA', 5);
INSERT INTO `kode_ref` VALUES (90, '81121', 2, 'INTAN AYU PERMATA SARI', 5);
INSERT INTO `kode_ref` VALUES (91, '70943', 2, 'NAJWA NAJUBA', 5);
INSERT INTO `kode_ref` VALUES (92, '87665', 2, 'RIMAS NIA AGUSTIN', 5);
INSERT INTO `kode_ref` VALUES (93, '62818', 2, 'TATIA NUR DIANA', 5);
INSERT INTO `kode_ref` VALUES (94, '70079', 2, 'ZAFITRI OCTAVIANI', 5);
INSERT INTO `kode_ref` VALUES (95, '27225', 2, 'ANNISA HARTANTI', 6);
INSERT INTO `kode_ref` VALUES (96, '95300', 2, 'DEVI ZASKIA TIARA R.', 6);
INSERT INTO `kode_ref` VALUES (97, '44802', 2, 'OLGA BERLIN AGUSTIN', 6);
INSERT INTO `kode_ref` VALUES (98, '70499', 2, 'SAFEBRILA', 6);
INSERT INTO `kode_ref` VALUES (99, '12436', 1, 'ABDURAHMAN', 7);
INSERT INTO `kode_ref` VALUES (100, '58804', 1, 'ACHMAD THORIQ AL-AYYUBI', 7);
INSERT INTO `kode_ref` VALUES (101, '51844', 1, 'BAGUS SANDIARTA', 7);
INSERT INTO `kode_ref` VALUES (102, '89455', 1, 'CAHYA INDRA LUKMANA', 7);
INSERT INTO `kode_ref` VALUES (103, '36710', 1, 'DANIEL DECO NAJHAN ZALFA', 7);
INSERT INTO `kode_ref` VALUES (104, '43302', 1, 'DHONI AHMAD MUHAJJIR', 7);
INSERT INTO `kode_ref` VALUES (105, '20195', 1, 'DZIKRU ROZAQ', 7);
INSERT INTO `kode_ref` VALUES (106, '81834', 1, 'ERLANGGA CAHYA PUTRA', 7);
INSERT INTO `kode_ref` VALUES (107, '77701', 1, 'FARIHIN IHSAN', 7);
INSERT INTO `kode_ref` VALUES (108, '27350', 1, 'M. DAWAM ROYHANUL MUKAROM', 7);
INSERT INTO `kode_ref` VALUES (109, '69585', 1, 'MOCHAMMAD GIAN HARJA YODHA', 7);
INSERT INTO `kode_ref` VALUES (110, '65417', 1, 'MOCHAMAD ROYKHAN KEN IZZUDDIN', 7);
INSERT INTO `kode_ref` VALUES (111, '86560', 1, 'MOCHAMMAD FAQIHUDDIN HUDA', 7);
INSERT INTO `kode_ref` VALUES (112, '51781', 1, 'MOKHAMAD ZULFIKAR PUTRA IMRON', 7);
INSERT INTO `kode_ref` VALUES (113, '85324', 1, 'MUKHAMAD AMIR AKHSANUL KHOLIQIN', 7);
INSERT INTO `kode_ref` VALUES (114, '61325', 1, 'NAIL MAJID SANTOSO', 7);
INSERT INTO `kode_ref` VALUES (115, '34486', 1, 'NAWWAL IRFAN MUHAMMAD', 7);
INSERT INTO `kode_ref` VALUES (116, '42648', 1, 'NOZA JIMMY PRASETYO', 7);
INSERT INTO `kode_ref` VALUES (117, '47968', 1, 'ROISUL AHYER', 7);
INSERT INTO `kode_ref` VALUES (118, '27045', 1, 'YUSRIL SAFRIL AHMADA', 7);
INSERT INTO `kode_ref` VALUES (119, '79561', 2, 'AGUSTI REVI NUHHA', 8);
INSERT INTO `kode_ref` VALUES (120, '36121', 2, 'AUHA MAY ZIADAH', 8);
INSERT INTO `kode_ref` VALUES (121, '79030', 2, 'DYAH RINI KUSUMAWATI', 8);
INSERT INTO `kode_ref` VALUES (122, '29521', 2, 'ASHA AZARIA', 8);
INSERT INTO `kode_ref` VALUES (123, '75712', 1, 'ACHMAD WAHYU AL-BAIHAQI', 9);
INSERT INTO `kode_ref` VALUES (124, '31505', 2, 'ADHELIA PUTRI ISLAMIAH', 10);
INSERT INTO `kode_ref` VALUES (125, '31379', 2, 'AMANDA RAHMAWATI', 10);
INSERT INTO `kode_ref` VALUES (126, '66661', 2, 'CHOIROTUL ULA', 10);
INSERT INTO `kode_ref` VALUES (127, '68262', 1, 'DHONNY EMRE ROY', 9);
INSERT INTO `kode_ref` VALUES (128, '55269', 1, 'DICKY PUTRA ARDIANSYAH', 9);
INSERT INTO `kode_ref` VALUES (129, '60334', 1, 'FIDI GAUTAMA VALENTINO ANTOLIN', 9);
INSERT INTO `kode_ref` VALUES (130, '31467', 1, 'FIRZHA IBRAHIM WIDYA NUGROHO', 9);
INSERT INTO `kode_ref` VALUES (131, '57100', 1, 'FITRA AL INSANI', 9);
INSERT INTO `kode_ref` VALUES (132, '78396', 1, 'HABIB MAS\'UD DJAZULI', 9);
INSERT INTO `kode_ref` VALUES (133, '99407', 2, 'HABIBAH SHOFI FUTUHIL AULIA', 10);
INSERT INTO `kode_ref` VALUES (134, '34929', 1, 'HASBI AFRIZAL FAHMI', 9);
INSERT INTO `kode_ref` VALUES (135, '70049', 1, 'MUHAMMAD ROIZU DZUL FIKRI', 9);
INSERT INTO `kode_ref` VALUES (136, '13862', 1, 'MUHAMMAD YASYFI FUADANA', 9);
INSERT INTO `kode_ref` VALUES (137, '66476', 2, 'NISRINA WASHFA NABILA', 10);
INSERT INTO `kode_ref` VALUES (138, '17485', 2, 'SHOFI SAVIRA', 10);
INSERT INTO `kode_ref` VALUES (139, '75534', 1, 'WILDAN SALASA', 9);
INSERT INTO `kode_ref` VALUES (140, '86047', 1, 'YAKI\' FATAHILLAH YULSTIAWAN', 9);
INSERT INTO `kode_ref` VALUES (141, '75192', 2, 'AINUN JAMILA', 11);
INSERT INTO `kode_ref` VALUES (142, '84128', 2, 'AULIA NABILATUZ ZAHRO AL MAWADDAH', 11);
INSERT INTO `kode_ref` VALUES (143, '52995', 2, 'CITRA CHINTIYA SARI', 11);
INSERT INTO `kode_ref` VALUES (144, '76135', 2, 'EKA FARIHATUL AFIANTI', 11);
INSERT INTO `kode_ref` VALUES (145, '78272', 2, 'KHUSNUL KHOTIMAH PUTRI WIJAYA', 11);
INSERT INTO `kode_ref` VALUES (146, '50752', 2, 'NADIA RAHMA PUTRI', 11);
INSERT INTO `kode_ref` VALUES (147, '76024', 2, 'NAILAL FAROHAH ABIDIN', 11);
INSERT INTO `kode_ref` VALUES (148, '40336', 2, 'NAZLATUS MASDHA', 11);
INSERT INTO `kode_ref` VALUES (149, '68359', 2, 'RENATA SYAKILLA ROHMATUS SYA\'DIYAH', 11);
INSERT INTO `kode_ref` VALUES (150, '22423', 2, 'UMI FARIDA ZULFA', 11);
INSERT INTO `kode_ref` VALUES (151, '58100', 2, 'YASINTA HASNA ANWAR', 11);
INSERT INTO `kode_ref` VALUES (152, '48919', 2, 'ZAKIYAH NAYLAL IZZAH', 11);
INSERT INTO `kode_ref` VALUES (153, '52854', 1, 'ACHMAD DAFFAA SYIFA\'UN FADHILLAH', 12);
INSERT INTO `kode_ref` VALUES (154, '15646', 1, 'ACHMAD DIAUDDIN', 12);
INSERT INTO `kode_ref` VALUES (155, '19528', 1, 'ACHMAD KOMARUDIN', 12);
INSERT INTO `kode_ref` VALUES (156, '81255', 1, 'ADAM RAHMATULLOH', 12);
INSERT INTO `kode_ref` VALUES (157, '79260', 1, 'AHMAD ABBAS FAIDILLAH', 12);
INSERT INTO `kode_ref` VALUES (158, '86446', 1, 'AHMAD FARHAN BINTARO ALAMSYAH', 12);
INSERT INTO `kode_ref` VALUES (159, '90229', 1, 'AHMAD WILDAN MAULANA', 12);
INSERT INTO `kode_ref` VALUES (160, '13225', 1, 'AKBAR HASIM', 12);
INSERT INTO `kode_ref` VALUES (161, '83878', 1, 'ALI ASHAR', 12);
INSERT INTO `kode_ref` VALUES (162, '67491', 1, 'DIMAS DWITYA KUSUMA', 12);
INSERT INTO `kode_ref` VALUES (163, '32848', 1, 'DURRY ABIYYU NAWWAF', 12);
INSERT INTO `kode_ref` VALUES (164, '48338', 1, 'DWI ILHAM TABAH SAMUDRA', 12);
INSERT INTO `kode_ref` VALUES (165, '24596', 1, 'FACHRIS FIRMAN RAMADHANI', 12);
INSERT INTO `kode_ref` VALUES (166, '72788', 1, 'HANIF ASKAR DZIKIER', 12);
INSERT INTO `kode_ref` VALUES (167, '54475', 1, 'IMAM GHOZALI', 12);
INSERT INTO `kode_ref` VALUES (168, '61723', 1, 'JEFRY SAPUTRO', 12);
INSERT INTO `kode_ref` VALUES (169, '60360', 1, 'MOH NAUFAL AD DAUFI', 12);
INSERT INTO `kode_ref` VALUES (170, '64299', 1, 'M. FERDY MAULIDAN', 12);
INSERT INTO `kode_ref` VALUES (171, '72868', 1, 'MOCHAMMAD RIZA WIDYA CHESTA ADABI', 12);
INSERT INTO `kode_ref` VALUES (172, '57837', 1, 'MUHAMMAD FATIH ZAMZANI', 12);
INSERT INTO `kode_ref` VALUES (173, '55607', 1, 'MUHAMMAD HAMZAH ALBATAMY', 12);
INSERT INTO `kode_ref` VALUES (174, '13502', 1, 'MOCH. NUKMAN ZAINUDDIN', 12);
INSERT INTO `kode_ref` VALUES (175, '36901', 1, 'MUKHAMMAD FARRID MUKHOIRROVI AKBAR', 12);
INSERT INTO `kode_ref` VALUES (176, '43835', 1, 'RIZKI AGUNG WIBOWO', 12);
INSERT INTO `kode_ref` VALUES (177, '84228', 1, 'SYAHRUL MUBAROQ', 12);
INSERT INTO `kode_ref` VALUES (178, '65616', 1, 'ACHMAD AUDI RIZKY', 13);
INSERT INTO `kode_ref` VALUES (179, '81879', 1, 'ACHMAD ZACKY', 13);
INSERT INTO `kode_ref` VALUES (180, '46730', 1, 'AMAR DJATI DIMAS NUGRAHA', 13);
INSERT INTO `kode_ref` VALUES (181, '84439', 1, 'BARIZUL HAQ AHMAD', 13);
INSERT INTO `kode_ref` VALUES (182, '91765', 1, 'CHAIS JUNASOR', 13);
INSERT INTO `kode_ref` VALUES (183, '95302', 1, 'CHOIRU MAZKURI RAHMAN', 13);
INSERT INTO `kode_ref` VALUES (184, '64638', 1, 'I\'TADUL MUROQIY', 13);
INSERT INTO `kode_ref` VALUES (185, '84421', 1, 'KHANAYA EL FARRADZ HARAHAP', 13);
INSERT INTO `kode_ref` VALUES (186, '94617', 1, 'M HANIF ZAMZAM', 13);
INSERT INTO `kode_ref` VALUES (187, '32950', 1, 'M. IRFAUR RIZKI', 13);
INSERT INTO `kode_ref` VALUES (188, '63060', 1, 'M. SEPTIAN RIZKY WIJAYA', 13);
INSERT INTO `kode_ref` VALUES (189, '34330', 1, 'MISBAHUL RAFLI ALFANDI', 13);
INSERT INTO `kode_ref` VALUES (190, '93907', 1, 'MOCH. THORIQ ADIL LAIDE KAMMARUZZAMAN KHOLIK', 13);
INSERT INTO `kode_ref` VALUES (191, '81148', 1, 'MOCHAMAD ULUL ALBAB', 13);
INSERT INTO `kode_ref` VALUES (192, '17080', 1, 'MOHAMMAD DZULKIFLI AFRIZAL AHMAD', 13);
INSERT INTO `kode_ref` VALUES (193, '20951', 1, 'MUHAMMAD HUSNI MUBAROK', 13);
INSERT INTO `kode_ref` VALUES (194, '48545', 1, 'MUHAMMAD MUHIBBIL \'ILMI', 13);
INSERT INTO `kode_ref` VALUES (195, '86527', 1, 'MUHAMMAD SYAIFUR RIDHO', 13);
INSERT INTO `kode_ref` VALUES (196, '82131', 1, 'MUHAMMAD VIKKY NURSYAHRONI', 13);
INSERT INTO `kode_ref` VALUES (197, '40345', 1, 'REVANZA PUTRA RAMADHANI', 13);
INSERT INTO `kode_ref` VALUES (198, '49520', 1, 'RIZKY AJI BAYU SAMPURNA', 13);
INSERT INTO `kode_ref` VALUES (199, '36123', 1, 'TEGAR ILYASA', 13);
INSERT INTO `kode_ref` VALUES (200, '93697', 2, 'AYU DUWI AGUSTINA', 14);
INSERT INTO `kode_ref` VALUES (201, '50102', 2, 'DEWI NAYLUL MUFIDAH', 14);
INSERT INTO `kode_ref` VALUES (202, '42551', 2, 'FADILA AULIA SARI', 14);
INSERT INTO `kode_ref` VALUES (203, '34984', 2, 'FATIMAH AZZAHRA', 14);
INSERT INTO `kode_ref` VALUES (204, '16956', 2, 'JENNY CITRA HANDAYANI', 14);
INSERT INTO `kode_ref` VALUES (205, '27388', 2, 'KHIKMATUL MAKHLUFI ANANTA', 14);
INSERT INTO `kode_ref` VALUES (206, '89188', 2, 'MAGHFIROH BARIDATUL AFWIYA', 14);
INSERT INTO `kode_ref` VALUES (207, '39998', 2, 'SYAYLA HANIF FAIZUNA', 14);
INSERT INTO `kode_ref` VALUES (208, '68449', 2, 'TSALITSATUL MAULIDAH', 14);
INSERT INTO `kode_ref` VALUES (209, '44171', 1, 'ADEL WIBISONO', 15);
INSERT INTO `kode_ref` VALUES (210, '72008', 1, 'AHMAD NURUDIN ISLAMI', 15);
INSERT INTO `kode_ref` VALUES (211, '78548', 1, 'AQIB IRVAN PURNOMO', 15);
INSERT INTO `kode_ref` VALUES (212, '80249', 1, 'MUHAMMAD AMRY YUSRON', 15);
INSERT INTO `kode_ref` VALUES (213, '70018', 1, 'MUKHAMAD FATKHUROHMAN', 15);
INSERT INTO `kode_ref` VALUES (214, '60428', 1, 'NANDA FRIMA SETIAWAN', 15);
INSERT INTO `kode_ref` VALUES (215, '97673', 2, 'ADITAMI FITRIA', 16);
INSERT INTO `kode_ref` VALUES (216, '50417', 2, 'ANASTASYA STEFANIE QURROTA A\'YUN', 16);
INSERT INTO `kode_ref` VALUES (217, '14705', 2, 'FAMELIA NAILUN NAJWA', 16);
INSERT INTO `kode_ref` VALUES (218, '33918', 2, 'FIORENTINA RAMADHANI', 16);
INSERT INTO `kode_ref` VALUES (219, '20431', 2, 'INDANA ZULFA', 16);
INSERT INTO `kode_ref` VALUES (220, '77482', 2, 'MAYANG AWRELIYA SAKHA SOMALLA', 16);
INSERT INTO `kode_ref` VALUES (221, '27465', 2, 'NAILLA KUSUMA YULIANT PUTRI', 16);
INSERT INTO `kode_ref` VALUES (222, '86948', 2, 'RANI LESTARI', 16);
INSERT INTO `kode_ref` VALUES (223, '53247', 2, 'SANIA NURFARIKHA', 16);
INSERT INTO `kode_ref` VALUES (224, '99983', 2, 'SAVIRA PUTRI NURHALISA', 16);
INSERT INTO `kode_ref` VALUES (225, '77090', 2, 'SIRLI TAHTA DHILALI', 16);
INSERT INTO `kode_ref` VALUES (226, '14720', 2, 'ZILDJIAN DZUN NUR\'AIN AZ-ZAHRA', 16);

-- ----------------------------
-- Table structure for voting
-- ----------------------------
DROP TABLE IF EXISTS `voting`;
CREATE TABLE `voting`  (
  `id_voting` int(11) NOT NULL AUTO_INCREMENT,
  `koderef` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `idevoting_fk` int(11) NULL DEFAULT NULL,
  `idcalon_fk` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id_voting`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 44 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of voting
-- ----------------------------
INSERT INTO `voting` VALUES (6, '25252', 1, 1);
INSERT INTO `voting` VALUES (7, '15375', 1, 1);
INSERT INTO `voting` VALUES (8, '51128', 1, 2);
INSERT INTO `voting` VALUES (9, '23000', 1, 1);
INSERT INTO `voting` VALUES (10, '40000', 1, 1);
INSERT INTO `voting` VALUES (11, '23966', 1, 2);
INSERT INTO `voting` VALUES (12, '86316', 1, 1);
INSERT INTO `voting` VALUES (13, '43932', 1, 2);
INSERT INTO `voting` VALUES (14, '89455', 1, 1);
INSERT INTO `voting` VALUES (15, '86711', 1, 1);
INSERT INTO `voting` VALUES (16, '12436', 1, 2);
INSERT INTO `voting` VALUES (17, '58804', 1, 1);
INSERT INTO `voting` VALUES (18, '67352', 1, 1);
INSERT INTO `voting` VALUES (19, '79593', 1, 2);
INSERT INTO `voting` VALUES (20, '40630', 1, 1);
INSERT INTO `voting` VALUES (21, '36710', 1, 1);
INSERT INTO `voting` VALUES (22, '34664', 1, 1);
INSERT INTO `voting` VALUES (23, '51844', 1, 2);
INSERT INTO `voting` VALUES (24, '20195', 1, 2);
INSERT INTO `voting` VALUES (25, '86150', 2, 3);
INSERT INTO `voting` VALUES (26, '56603', 2, 3);
INSERT INTO `voting` VALUES (27, '75537', 2, 3);
INSERT INTO `voting` VALUES (28, '89961', 2, 3);
INSERT INTO `voting` VALUES (29, '92218', 2, 4);
INSERT INTO `voting` VALUES (30, '90712', 2, 4);
INSERT INTO `voting` VALUES (31, '85066', 2, 3);
INSERT INTO `voting` VALUES (32, '32648', 2, 4);
INSERT INTO `voting` VALUES (33, '87665', 2, 4);
INSERT INTO `voting` VALUES (34, '62818', 2, 3);
INSERT INTO `voting` VALUES (35, '44802', 2, 3);
INSERT INTO `voting` VALUES (36, '95300', 2, 4);
INSERT INTO `voting` VALUES (37, '43302', 1, 1);
INSERT INTO `voting` VALUES (38, '95158', 2, 3);
INSERT INTO `voting` VALUES (39, '70079', 2, 4);
INSERT INTO `voting` VALUES (40, '27225', 2, 3);
INSERT INTO `voting` VALUES (41, '70499', 2, 4);
INSERT INTO `voting` VALUES (42, '81834', 1, 1);
INSERT INTO `voting` VALUES (43, '57375', 2, 3);

SET FOREIGN_KEY_CHECKS = 1;
