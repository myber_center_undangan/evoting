<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class vote extends CI_Controller {
	

	/*
		CHANGE PAGE
	*/
	public function event($kode="")
	{
		if ($kode !== "") {
			$data['evoting']	=	$this->db->get_where('evoting', ['kode'=>$kode])->row_array();
			$data['calon']		=	$this->db->get_where('calon', ['idevoting_fk'=>$data['evoting']['id_evoting']])->result_array();
			if ($kode == 'p1') {
				$this->load->view('role/vote/index', $data);
			}else{
				$this->load->view('role/vote/index', $data);
			}
			$this->load->view('role/vote/js');
			// code...
		}
	}


	public function countable($kode="")
	{
		if ($kode !== "") {
			$data['evoting']		=	$this->db->get_where('evoting', ['kode'=>$kode])->row_array();
			$data['calon']			=	$this->db->get_where('calon', ['idevoting_fk'=>$data['evoting']['id_evoting']])->result_array();
			$data['total_voting']	=	$this->db->get_where('voting', ['idevoting_fk'=>$data['evoting']['id_evoting']])->num_rows();
			$data['total_responden']	=	$this->db->get_where('kode_ref', ['idevoting_fk'=>$data['evoting']['id_evoting']])->num_rows();
			$data['responden_pilih']	=	$this->db->query("SELECT count(*) as val FROM kode_ref where idevoting_fk = ".$data['evoting']['id_evoting']." and  kode_ref.kode  in (select koderef from voting)")->row_array();
			$data['responden_tidak_pilih']	=	$this->db->query("SELECT count(*) as val FROM kode_ref where idevoting_fk = ".$data['evoting']['id_evoting']." and kode_ref.kode not in (select koderef from voting)")->row_array();
			$data['calon_voting'] 	= [];

			foreach ($data['calon'] as $key => $value) {
				$total_voting_calon = 	$this->db->get_where('voting', ['idcalon_fk'=>$value['id_calon'], 'idevoting_fk'=>$data['evoting']['id_evoting']])->num_rows();
				$persentase 		=	($total_voting_calon/$data['total_voting']) * 100;
				// $persentase = 0;
				// if ($value['id_calon'] == 1) {
				// 	$persentase = 60;
				// 	$data['calon_voting'][] = [
				// 		'calon' 				=>	$value,
				// 		'total_voting_calon' 	=> 	round($data['responden_pilih']['val']*0.6),
				// 		'persentase'			=>	$persentase
				// 	];
				// }else if($value['id_calon'] == 2) {
				// 	$persentase = 40;
				// 	$data['calon_voting'][] = [
				// 		'calon' 				=>	$value,
				// 		'total_voting_calon' 	=> 	round($data['responden_pilih']['val']*0.4),
				// 		'persentase'			=>	$persentase
				// 	];
				// }else if($value['id_calon'] == 3) {
				// 	$persentase = 55;
				// 	$data['calon_voting'][] = [
				// 		'calon' 				=>	$value,
				// 		'total_voting_calon' 	=> 	round($data['responden_pilih']['val']*0.55),
				// 		'persentase'			=>	$persentase
				// 	];
				// }else if($value['id_calon'] == 4) {
				// 	$persentase = 45;
				// 	$data['calon_voting'][] = [
				// 		'calon' 				=>	$value,
				// 		'total_voting_calon' 	=> 	round($data['responden_pilih']['val']*0.45),
				// 		'persentase'			=>	$persentase
				// 	];
				// }
				$data['calon_voting'][] = [
					'calon' 				=>	$value,
					'total_voting_calon' 	=> 	$total_voting_calon,
					'persentase'			=>	$persentase
				];
			}
			// print_r($data['total_responden']);
			$this->load->view('role/vote/countable', $data);
			$this->load->view('role/vote/js_count');
		}
	}

	public function quick_count($kode="")
	{
		if ($kode !== "") {
			$data['evoting']		=	$this->db->get_where('evoting', ['kode'=>$kode])->row_array();
			$data['kode']			=	$kode;
			$data['calon']			=	$this->db->get_where('calon', ['idevoting_fk'=>$data['evoting']['id_evoting']])->result_array();
			$data['total_voting']	=	$this->db->get_where('voting', ['idevoting_fk'=>$data['evoting']['id_evoting']])->num_rows();
			$data['total_responden']	=	$this->db->get_where('kode_ref', ['idevoting_fk'=>$data['evoting']['id_evoting']])->num_rows();
			$data['responden_pilih']	=	$this->db->query("SELECT count(*) as val FROM kode_ref where idevoting_fk = ".$data['evoting']['id_evoting']." and  kode_ref.kode  in (select koderef from voting)")->row_array();
			$data['responden_tidak_pilih']	=	$this->db->query("SELECT count(*) as val FROM kode_ref where idevoting_fk = ".$data['evoting']['id_evoting']." and kode_ref.kode not in (select koderef from voting)")->row_array();
			$data['calon_voting'] 	= [];

			foreach ($data['calon'] as $key => $value) {
				$total_voting_calon = 	$this->db->get_where('voting', ['idcalon_fk'=>$value['id_calon'], 'idevoting_fk'=>$data['evoting']['id_evoting']])->num_rows();
				$persentase 		=	($total_voting_calon/$data['total_voting']) * 100;

				$data['calon_voting'][] = [
					'calon' 				=>	$value,
					'total_voting_calon' 	=> 	$total_voting_calon,
					'persentase'			=>	$persentase
				];
			}
			// print_r($data['total_responden']);
			$this->load->view('role/vote/quick_count', $data);
			$this->load->view('role/vote/js_quick_count');
		}
	}

	function get_quick($kode=""){
		if ($kode !== "") {
			$data['evoting']		=	$this->db->get_where('evoting', ['kode'=>$kode])->row_array();
			$data['kode']			=	$kode;
			$data['calon']			=	$this->db->get_where('calon', ['idevoting_fk'=>$data['evoting']['id_evoting']])->result_array();
			$data['total_voting']	=	$this->db->get_where('voting', ['idevoting_fk'=>$data['evoting']['id_evoting']])->num_rows();
			$data['total_responden']	=	$this->db->get_where('kode_ref', ['idevoting_fk'=>$data['evoting']['id_evoting']])->num_rows();
			$data['responden_pilih']	=	$this->db->query("SELECT count(*) as val FROM kode_ref where idevoting_fk = ".$data['evoting']['id_evoting']." and  kode_ref.kode  in (select koderef from voting)")->row_array();
			$data['responden_tidak_pilih']	=	$this->db->query("SELECT count(*) as val FROM kode_ref where idevoting_fk = ".$data['evoting']['id_evoting']." and kode_ref.kode not in (select koderef from voting)")->row_array();
			$data['responden_terakhir']	=	$this->db->query('select *, (select siswa from kode_ref where kode_ref.kode = koderef) as siswa from voting  ORDER BY id_voting desc limit 5')->result_array();
			$data['calon_voting'] 	= [];

			foreach ($data['calon'] as $key => $value) {
				$total_voting_calon = 	$this->db->get_where('voting', ['idcalon_fk'=>$value['id_calon'], 'idevoting_fk'=>$data['evoting']['id_evoting']])->num_rows();
				$persentase 		=	($total_voting_calon/$data['total_voting']) * 100;

				$data['calon_voting'][] = [
					'calon' 				=>	$value,
					'total_voting_calon' 	=> 	$total_voting_calon,
					'persentase'			=>	$persentase
				];
			}
			// print_r($data['total_responden']);
			$this->load->view('role/vote/content_quick', $data);
		}
	}

	function set_selected(){
		$send = [];
		$cek = $this->db->get_where('kode_ref', ['kode'=>$_POST['koderef'], 'idevoting_fk'=>$_POST['id_evoting']]);
		if ($cek->num_rows() > 0) {
			$cekkode = $this->db->get_where('voting', ['koderef'=>$_POST['koderef'], 'idevoting_fk'=>$_POST['id_evoting']]);
			if ($cekkode->num_rows() == 0) {

				$this->db->insert('voting', ['koderef'=>$_POST['koderef'], 'idevoting_fk'=>$_POST['id_evoting'], 'idcalon_fk'=>$_POST['id']]);

				$send = [
					'status' =>	1,
					'msg'	=>	'Berhasil memilih paslon'
				];
			}else{
				$send = [
					'status' =>	0,
					'msg'	=>	'Kode Sudah Digunakan'
				];
			}
		}else{
			$send = [
				'status' =>	0,
				'msg'	=>	'Kode Tidak Ditemukan'
			];
		}
		echo json_encode($send);
	}
}