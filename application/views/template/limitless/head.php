<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title><?php echo $profil_website['nama_website'] ?></title>

  <!-- Global stylesheets -->
  <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
  <link href="<?php echo base_url('include/template/limitless2/')?>global_assets/css/icons/icomoon/styles.css" rel="stylesheet" type="text/css">
  <link href="<?php echo base_url('include/template/limitless2/')?>assets/css/bootstrap.min.css" rel="stylesheet" type="text/css">
  <link href="<?php echo base_url('include/template/limitless2/')?>assets/css/core.min.css" rel="stylesheet" type="text/css">
  <link href="<?php echo base_url('include/template/limitless2/')?>assets/css/components.min.css" rel="stylesheet" type="text/css">
  <link href="<?php echo base_url('include/template/limitless2/')?>assets/css/colors.min.css" rel="stylesheet" type="text/css">

  <link rel="stylesheet" href="<?php echo base_url('include/template/toastr/toastr.css')?>">
  <!-- /global stylesheets -->
  <script src="https://www.gstatic.com/firebasejs/8.3.1/firebase-app.js"></script>
<script src="https://www.gstatic.com/firebasejs/8.3.1/firebase-analytics.js"></script>
<script src="https://www.gstatic.com/firebasejs/8.3.1/firebase-auth.js"></script>
<script src="https://www.gstatic.com/firebasejs/8.3.1/firebase-database.js"></script>
  <!-- Core JS files -->
  <script src="<?php echo base_url('include/template/limitless2/')?>global_assets/js/plugins/loaders/pace.min.js"></script>
  <script src="<?php echo base_url('include/template/limitless2/')?>global_assets/js/core/libraries/jquery.min.js"></script>
  <script src="<?php echo base_url('include/template/limitless2/')?>global_assets/js/core/libraries/bootstrap.min.js"></script>
  <script src="<?php echo base_url('include/template/limitless2/')?>global_assets/js/plugins/loaders/blockui.min.js"></script>

  <script src="<?php echo base_url('include/template/limitless2/')?>global_assets/js/plugins/notifications/noty.min.js"></script>
  <script src="<?php echo base_url('include/template/limitless2/')?>global_assets/js/plugins/notifications/jgrowl.min.js"></script>
  <!-- /core JS files -->
  
  
  <!-- Theme JS files -->
  <script src="<?php echo base_url('include/template/limitless2/')?>assets/js/app.js"></script>
  <link rel="stylesheet" href="<?php echo base_url('include/template/toastr/toastr.css')?>">
<script src="<?php echo base_url('include/template/limitless2/')?>global_assets/js/plugins/ui/ripple.min.js"></script>
  <link rel="stylesheet" href="<?php echo base_url('include/core/core.css')?>">
  <script src="<?php echo base_url('include/core/core.js')?>"></script>
  
<script src="<?php echo base_url('include/template/toastr/toastr.min.js')?>"></script>
<script src="<?php echo base_url('include/template/keys/jquery.key.js')?>"></script>

  <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAz51lKipef50F9xznI-1g_O0GQt0JR7h0"></script>
  <!-- /theme JS files -->

</head>