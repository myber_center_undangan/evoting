<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title></title>
	<!-- Global stylesheets -->
  <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
  <link href="<?php echo base_url('include/template/limitless2/')?>global_assets/css/icons/icomoon/styles.css" rel="stylesheet" type="text/css">
  <link href="<?php echo base_url('include/template/limitless2/')?>assets/css/bootstrap.min.css" rel="stylesheet" type="text/css">
  <link href="<?php echo base_url('include/template/limitless2/')?>assets/css/core.min.css" rel="stylesheet" type="text/css">
  <link href="<?php echo base_url('include/template/limitless2/')?>assets/css/components.min.css" rel="stylesheet" type="text/css">
  <link href="<?php echo base_url('include/template/limitless2/')?>assets/css/colors.min.css" rel="stylesheet" type="text/css">

	<link rel="stylesheet" type="text/css" href="<?php echo base_url('include/') ?>style.css">
  <link rel="stylesheet" href="<?php echo base_url('include/template/toastr/toastr.css')?>">
  <!-- Core JS files -->
  <script src="<?php echo base_url('include/template/limitless2/')?>global_assets/js/plugins/loaders/pace.min.js"></script>
  <script src="<?php echo base_url('include/template/limitless2/')?>global_assets/js/core/libraries/jquery.min.js"></script>
  <script src="<?php echo base_url('include/template/limitless2/')?>global_assets/js/core/libraries/bootstrap.min.js"></script>
  <script src="<?php echo base_url('include/template/limitless2/')?>global_assets/js/plugins/loaders/blockui.min.js"></script>

  <script src="<?php echo base_url('include/template/limitless2/')?>global_assets/js/plugins/notifications/noty.min.js"></script>
  <script src="<?php echo base_url('include/template/limitless2/')?>global_assets/js/plugins/notifications/jgrowl.min.js"></script>
  <!-- /core JS files -->
  
  
  <!-- Theme JS files -->

	<script src="<?php echo base_url('include/template/limitless2/')?>/global_assets/js/plugins/loaders/progressbar.min.js"></script>
  <script src="<?php echo base_url('include/template/limitless2/')?>assets/js/app.js"></script>
  <link rel="stylesheet" href="<?php echo base_url('include/template/toastr/toastr.css')?>">
<script src="<?php echo base_url('include/template/limitless2/')?>global_assets/js/plugins/ui/ripple.min.js"></script>
  <link rel="stylesheet" href="<?php echo base_url('include/core/core.css')?>">
  <script src="<?php echo base_url('include/core/core.js')?>"></script>
  
<script src="<?php echo base_url('include/template/toastr/toastr.min.js')?>"></script>
<script src="<?php echo base_url('include/template/keys/jquery.key.js')?>"></script>

  <!-- /theme JS files -->

</head>
<body style="background: url('<?php echo base_url("include/bg.jpg")?>');background-repeat: no-repeat;background-size: cover;">
	<div class="container">
			<h4 class="title-text">E-Voting</h4>
			<h4 class="title-text-body">SMK IT Asy-Syadzili</h4>
			<p class="title-text-p">Periode 2022/2023</p>
			<div class="card3">
				<div class="col-md-8">
					
				<div class="panel pnl-save panel-white">
								<div class="panel-heading">
									<h6 class="panel-title text-semibold">Perhitungan Suara</h6>
								</div>

								<ul class="media-list">
									<?php foreach ($calon_voting as $key => $value): ?>
									<li class="media panel-body stack-media-on-mobile">
										<div class="media-left">
											<a href="#">
												<img src="<?php echo base_url('template/'.$value['calon']['foto']) ?>" class="img-rounded img-lg" alt="">
											</a>
										</div>

										<div class="media-body">
											<h6 class="media-heading text-semibold">
												<a href="#"><?php echo $value['calon']['nama'] ?></a>
											</h6>

											<ul style="display:none;" class="list-inline hide_none list-inline-separate text-muted mb-10">
												<li>Total Suara : <?php echo $total_voting ?></li>
												<li>Total Pemilih : <?php echo $value['total_voting_calon'] ?></li>
											</ul>

											<div class="progress content-group-sm" id="h-center-basic<?php echo $value['calon']['id_calon'] ?>">
												<div class="progress-bar progress-bar-primary" data-transitiongoal-backup="<?php echo $value['persentase'] ?>" data-transitiongoal="<?php echo $value['persentase'] ?>" style="width: 0%">
													<span class="sr-only">0%</span>
												</div>
											</div>
										</div>

										
									</li>
									<?php endforeach ?>
								</ul>
							</div>
							<center><button id="h-center-basic-start" class="btn btn-success">Mulai Penghitungan Suara</button></center>
				</div>
				<div class="col-md-4">
					<div class="panel pnl-save panel-white">
						<div class="panel-heading">
									<h6 class="panel-title text-semibold">Perhitungan Suara</h6>
						</div>
						<div class="panel-body">
							<ul class="list-group">
							  <li class="list-group-item d-flex justify-content-between align-items-center">
							    Total Responden
							    <span class="badge badge-primary badge-pill"><?php echo $total_responden ?></span>
							  </li>
							  <li class="list-group-item d-flex justify-content-between align-items-center">
							    Responden yang sudah memilih
							    <span class="badge badge-primary badge-pill"><?php echo $responden_pilih['val'] ?></span>
							  </li>
							  <li class="list-group-item d-flex justify-content-between align-items-center">
							    Responden yang belum memilih
							    <span class="badge badge-danger badge-pill"><?php echo $responden_tidak_pilih['val'] ?></span>
							  </li>
							  <li class="list-group-item d-flex justify-content-between align-items-center">
							    
							  </li>
							</ul>
						</div>
					</div>
				</div>

			

	
			</div>
	</div>
	<footer class="text-footer">
		<marquee>Hallo, Selamat datang di e-voting smk, credit by Cyber Class</marquee>
	</footer>
	
