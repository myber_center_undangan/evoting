<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('include/') ?>style.css">
	<link href="<?php echo base_url('include/template/limitless2/')?>assets/css/bootstrap.min.css" rel="stylesheet" type="text/css">
  	
  <script src="<?php echo base_url('include/template/limitless2/')?>global_assets/js/core/libraries/jquery.min.js"></script>
  <script src="<?php echo base_url('include/template/limitless2/')?>global_assets/js/core/libraries/bootstrap.min.js"></script>

  <link rel="stylesheet" href="<?php echo base_url('include/core/core.css')?>">
  <script src="<?php echo base_url('include/core/core.js')?>"></script>
	<title></title>
</head>
<body style="background: url('<?php echo base_url("include/bg.jpg")?>');background-repeat: no-repeat;background-size: cover;">
	<div class="container">
			<!-- <img src="<?php echo base_url() ?>/template/1header.png" class="img-header"> -->
			<!-- <h4 class="title-text">E-Voting</h4>
			<h4 class="title-text-body">SMK IT Asy-Syadzili</h4>
			<p class="title-text-p">Periode 2023/2024</p> -->
			<div class="pos-card">
				<?php $no = 0; ?>
				<?php foreach ($calon as $key => $value): ?>
					
				
				<div class="card<?php echo ++$no ?>">

					<img class="img-card" src="<?php echo base_url() ?>/template/rectangle2.png"></img>
					
					<div class="img-sub">
						<div class="title-sub">
							<?php echo strtoupper($value['nama']) ?>
						</div>					
						<center>
							<img style="width: 90%;" src="<?php echo base_url() ?>/template/<?php echo $value['foto'] ?>"></img>
						</center>
					</div>
					<button type="button" data-id="<?php echo $value['id_calon'] ?>" data-foto="<?php echo base_url() ?>/template/<?php echo $value['foto'] ?>" data-calon="<?php echo strtoupper($value['nama']) ?>" class="btn-pick btn-all btn-nomin">Pilih</button>
				</div>
				
				<?php endforeach ?>
			</div>
			<div class="content-footer">
				<!-- <img src="template/Untitled-2.png" class="img-footer"> -->
			</div>

			
			<!-- <img class="img-card2" src="template/rectangle2.png"></img>
			<button class="btn-pick-2 btn-all">Pilih</button> -->
	</div>
	<footer class="text-footer">
		<marquee>Hallo, Selamat datang di e-voting smk, credit by Cyber Class SMK IT Asy-Syadzili</marquee>
	</footer>
	
	<div id="modal_animation" class="modal" tabindex="-1">
					<div class="modal-dialog">
						<div class="modal-content">
							
							<div class="modal-body">
								<center>
								<h6 class="text-semibold nama-calon"></h6>
								<img class="foto-calon img-responsive" style="max-width: 200px;margin-top: 2%;margin-bottom: 2%;" alt="">
								<p>Dengan ini saya menyetujui bahwasanya saya telah memilih nominasi yang ditetapkan secara sadar dan tidak dalam paksaan</p>

								<hr>
								</center>
								<br>
								<div class="msg"></div>
								<input type="hidden" value="<?php echo $evoting['id_evoting'] ?>" class="id_evoting" name="">
								<input type="hidden" class="idcalon" name="">
								<input type="text" class="form-control koderef" placeholder="Kode Referensi" name="">

							</div>

							<div class="modal-footer">
								<button type="button" class="btn btn-danger" data-dismiss="modal">Batalkan Pilihan</button>
								<button type="button" class="btn btn-primary btn-setuju">Setuju</button>
							</div>
						</div>
					</div>
				</div>

				<div id="modal_berhasil" class="modal" tabindex="-1">
					<div class="modal-dialog">
						<div class="modal-content">
							
							<div class="modal-body">
								<center>
								<h3 class="text-semibold text-success">Hai... Anda telah berhasil melakukan voting.</h3>
								</center>
							</div>

							<div class="modal-footer">
								<button type="button" class="btn btn-danger" data-dismiss="modal">Tutup</button>
							</div>
						</div>
					</div>
				</div>



