<script type="text/javascript">
	$('.btn-nomin').on('click', function(e){
		$('.nama-calon').text($(this).data('calon'));
		$('.foto-calon').attr('src', $(this).data('foto'));
		$('.idcalon').val($(this).data('id'));
		$('.koderef').val('');
		$('.msg').html('');
		$('#modal_animation').modal('toggle');
	});

	$('.btn-setuju').on('click', function(e){
		send_ajax('<?php echo base_url('vote/set_selected') ?>', {
			id: $('.idcalon').val(),
			koderef : $('.koderef').val(),
			id_evoting : $('.id_evoting').val()
		}).then(function(data){
			var resp = JSON.parse(data);

			if (resp.status == 0) {
				$('.msg').html('<div class="alert alert-danger"><b>Warning : '+resp.msg+'</b></div>');
			}else{
				$('#modal_animation').modal('toggle');
				$('#modal_berhasil').modal('toggle');
			}
		});
	})
</script>
</body>
</html>