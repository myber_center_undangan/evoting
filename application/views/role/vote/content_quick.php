<div class="col-md-4">
					<div class="panel pnl-save panel-white" style="min-height:350px;">
						<div class="panel-heading">
									<h6 class="panel-title text-semibold">Statistik</h6>
						</div>
						<div class="panel-body">
							<ul class="list-group">
							  <li class="list-group-item d-flex justify-content-between align-items-center">
							    Total Responden
							    <span class="badge badge-primary badge-pill"><?php echo $total_responden ?></span>
							  </li>
							  <li class="list-group-item d-flex justify-content-between align-items-center">
							    Responden yang sudah memilih
							    <span class="badge badge-primary badge-pill"><?php echo $responden_pilih['val'] ?></span>
							  </li>
							  <li class="list-group-item d-flex justify-content-between align-items-center">
							    Responden yang belum memilih
							    <span class="badge badge-danger badge-pill"><?php echo $responden_tidak_pilih['val'] ?></span>
							  </li>
							  <li class="list-group-item d-flex justify-content-between align-items-center">
							    
							  </li>
							</ul>
						</div>
					</div>
				</div>
				<div class="col-md-4">
					
							<div class="panel pnl-save panel-white" style="min-height:350px;">
								<div class="panel-heading">
									<h6 class="panel-title text-semibold">Prediksi</h6>
								</div>

								<ul class="media-list">
									<?php foreach ($calon_voting as $key => $value): ?>
									<li class="media panel-body stack-media-on-mobile">
										<div class="media-left">
											<a href="#">
												<img src="<?php echo base_url('template/'.$value['calon']['foto']) ?>" class="img-rounded img-lg" alt="">
											</a>
										</div>

										<div class="media-body">
											<h6 class="media-heading text-semibold">
												<a href="#"><?php echo $value['calon']['nama'] ?></a>
											</h6>


											<div class="progress">
											  <div class="progress-bar" role="progressbar" style="width: <?php echo round($value['persentase']) ?>%;" aria-valuenow="<?php echo round($value['persentase']) ?>" aria-valuemin="0" aria-valuemax="100"><?php echo round($value['persentase']) ?>%</div>
											</div>
										</div>

										
									</li>
									<?php endforeach ?>
								</ul>
							</div>
				</div>
				
				<div class="col-md-4">
					<div class="panel pnl-save panel-white" style="min-height:350px;">
						<div class="panel-heading">
									<h6 class="panel-title text-semibold">3 Responden Terakhir</h6>
						</div>
						<div class="panel-body">
							<ul class="list-group">
								<?php foreach ($responden_terakhir as $key => $value): ?>
  								<li class="list-group-item"> <b><?php echo "(".$value['koderef'].") ".$value['siswa'] ?></b></li>
								<?php endforeach ?>
							 
							</ul>
						</div>
					</div>
				</div>