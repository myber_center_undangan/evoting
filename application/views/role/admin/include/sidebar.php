<ul class="navigation navigation-main navigation-accordion">
  <!-- Main -->
  <li class="navigation-header"><span>Main</span> <i class="icon-menu" title="Main pages"></i></li>
  <li><a href="Dashboard/get_data" class="app-item"><i class="icon-home4"></i> <span>Dashboard</span></a></li>
  
  <li>
    <a href="#"><i class="icon-magazine"></i> <span>Administrasi</span></a>
    <ul>
      <li><a href="Prestasi_siswa/get_data" class="app-item">Prestasi Siswa</a></li>
      <li><a href="Buku_tamu/get_data" class="app-item">Buku Tamu</a></li>
      <li><a href="Surat_masuk/get_data" class="app-item">Surat Masuk</a></li>
      <li><a href="Surat_keluar/get_data" class="app-item">Surat Keluar</a></li>
      <li><a href="Kepegawaian/get_data" class="app-item">Buku Induk Kepegawaian</a></li>
    </ul>
  </li>

  <li>
    <a href="#"><i class="icon-coin-dollar"></i> <span>Keuangan</span></a>
    <ul>
      <li><a href="coa/get_data" class="app-item">Chart of Account</a></li>
      <li><a href="Penerimaan/spp" class="app-item">Pembayaran SPP</a></li>
      <li><a href="Penerimaan/tanggungan" class="app-item">Tanggungan Siswa</a></li>
      <li><a href="Pembayaran/get_data" class="app-item">Pembayaran Lain</a></li>
    </ul>
  </li>
  
  <li>
    <a href="#"><i class="icon-notebook"></i> <span>Bimbingan Konseling</span></a>
    <ul>
      <li><a href="Buku_pemanggilan_siswa/get_data" class="app-item">Buku Pemanggilan Siswa</a></li>
      <li><a href="Pelanggaran_siswa/get_data" class="app-item">Kartu Kendali Siswa</a></li>
      <li><a href="Alumni/get_data" class="app-item">Buku Alumni</a></li>
      <li><a href="Pelanggaran_siswa/poin_pelanggaran_setup" class="app-item">Point Pelanggaran</a></li>
    </ul>
  </li>

  

  <li>
    <a href="#"><i class="icon-book"></i> <span>PPDB</span></a>
    <ul>
      <li><a href="Ppdb/get_data" class="app-item">Data Pendaftar Baru</a></li>
      <li><a href="Ppdb/list" class="app-item">List Pendaftar Baru</a></li>
    </ul>
  </li>

 
  <!-- Page kits -->
  <li class="navigation-header"><span>Manajemen Kepegawaian</span> <i class="icon-menu" title="Page kits"></i></li>
   <li>
    <a href="#"><i class=" icon-paste4"></i> <span>Penilaian Kinerja Guru</span></a>
    <ul>
      <li><a href="Indikator_pkg/get_data" class="app-item">Indikator PKG</a></li>
      <li><a href="Penilaian_kinerja_guru/get_data" class="app-item">Input Data PKG</a></li>
      <li><a href="Penilaian_kinerja_guru/get_data_bulanan" class="app-item">Input Data PKG Bulanan</a></li>
      <li><a href="Penilaian_kinerja_guru/report_pkg" class="app-item">Report PKG</a></li>
    </ul>
  </li>
  <li>
    <a href="#"><i class=" icon-paste4"></i> <span>Presensi Guru</span></a>
    <ul>
      <li><a href="Presensi_guru/get_data" class="app-item">Presensi Guru</a></li>
      <li><a href="Presensi_guru/rekap" class="app-item">Rekap Presensi Guru</a></li>
    </ul>
  </li>
  <li>
    <a href="#"><i class=" icon-paste4"></i> <span>Data Pegawai</span></a>
    <ul>
      <li><a href="Kepegawaian/get_data" class="app-item">Buku Induk Kepegawaian</a></li>
      <li><a href="Jadwal_guru/get_data" class="app-item">Jadwal Guru</a></li>
      <li><a href="Persentase_guru/get_data" class="app-item">Data Persentase Guru</a></li>
    </ul>
  </li>
  <li>
    <a href="#"><i class="icon-paste4"></i> <span>Buku Kerja Guru</span></a>
    <ul>
      <li><a href="Kd/get_data" class="app-item">Identifikasi KI/KD</a></li>
      <li><a href="Rpp/get_data" class="app-item">Silabus</a></li>
      <li><a href="Rpp/get_data" class="app-item">Rpp</a></li>
      <li><a href="Prota/get_data" class="app-item">Prota</a></li>
      <li><a href="Promes/get_data" class="app-item">Promes</a></li>
      <li><a href="Banksoal/get_data" class="app-item">Bank soal</a></li>
    </ul>
  </li>
  <!-- Kurikulum -->
  <li class="navigation-header"><span>Kurikulum</span> <i class="icon-menu" title="Page kits"></i></li>
  <li>
    <a href="#"><i class=" icon-book"></i> <span>Data Pelajaran</span></a>
    <ul>
      <li><a href="Mata_pelajaran/get_data" class="app-item">Mata Pelajaran</a></li>
      <li><a href="Guru_mapel/get_kelas" class="app-item">Mapel per Kelas</a></li>
      <li><a href="Guru_mapel/get_data" class="app-item">Guru Mapel</a></li>
    </ul>
  </li>
  <li>
    <a href="#"><i class=" icon-book"></i> <span>Jadwal Pelajaran</span></a>
    <ul>
      <li><a href="Jam_pelajaran/get_data" class="app-item">Setting Jam Pelajaran</a></li>
      <li><a href="Jadwal_pelajaran/get_data" class="app-item">Jadwal Pelajaran per Kelas</a></li>
    </ul>
  </li>
  <li>
    <a href="#"><i class=" icon-book"></i> <span>Pengaturan Lain</span></a>
    <ul>
      <li><a href="Wali_kelas/get_data" class="app-item">Setting Wali Kelas</a></li>
    </ul>
  </li>
  <li>
   <!-- Kesiswaan -->
   <li class="navigation-header"><span>Kesiswaan</span> <i class="icon-menu" title="Page kits"></i></li>
   <li>
      <a href="#"><i class="icon-user-check"></i> <span>Data Siswa</span></a>
      <ul>
        <li><a href="Siswa/get_data" class="app-item">Semua Siswa</a></li>
        <li><a href="Siswa/perkelas" class="app-item">Data Siswa Perkelas</a></li>
        <li><a href="Presensi_harian/presensi_siswa" class="app-item">Presensi Siswa</a></li>
      </ul>
    </li>
    <li>
      <a href="#"><i class="icon-user-check"></i> <span>Laporan</span></a>
      <ul>
        <li><a href="Presensi_harian/rekap" class="app-item">Rekap Presensi Siswa</a></li>
      </ul>
    </li>
   <!-- /Kesiswaan -->
  <li class="navigation-header"><span>External App</span> <i class="icon-menu" title="Page kits"></i></li>
  <li>
    <a href="#"><i class=" icon-paste4"></i> <span>Online Assesment System</span></a>
    <ul>
      <li><a href="Oas/get_data" class="app-item">Tambah Jadwal OAS</a></li>
    </ul>
  </li>
  <li>
    <a href="#"><i class=" icon-paste4"></i> <span>E-Rapor</span></a>
    <ul>
      <li><a href="Erapor/get_data" class="app-item">Tambah Jadwal E-rapor</a></li>
    </ul>
  </li>
  <li class="navigation-header"><span>Setting kits</span> <i class="icon-menu" title="Page kits"></i></li>
  <li>
    <a href="#"><i class="icon-stack2"></i> <span>Master</span></a>
    <ul>
      <li><a href="Siswa/get_data" class="app-item">Siswa</a></li>
      <li><a href="Siswa_kelas/get_data" class="app-item">Daftar Siswa Kelas</a></li>
      <li><a href="Guru/get_data" class="app-item">Guru</a></li>
      <li><a href="Kelas/get_data" class="app-item">Kelas</a></li>
      <li><a href="Jurusan/get_data" class="app-item">Jurusan</a></li>
      <li><a href="Mata_pelajaran/get_data" class="app-item">Mata Pelajaran</a></li>
      <li><a href="Tahun_ajaran/get_data" class="app-item">Tahun ajaran</a></li>
      <li><a href="Guru_mapel/get_data" class="app-item">Guru Mapel</a></li>
    </ul>
  </li>
  <li>
    <a href="#"><i class="icon-users2"></i> <span>Account</span></a>
    
    <ul>
      <li><a href="users/get_data" class="app-item">User</a></li>
      <li><a href="Profil_website/get_data" class="app-item">Role</a></li>
  </li>
  <li><a href="Media/get_data" class="app-item"><i class="icon-stack-picture"></i> <span>Media</span></a></li>
  <!-- /page kits -->
</ul>